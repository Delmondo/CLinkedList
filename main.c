#include <stdio.h>
#include <stdlib.h>

#include "linkedlist.h"

int main(int argc, char *argv[])
{
	linkedlist_t *list = new_linkedlist();
	
	int data = 3;
	linkedlist_add(list, &data);
	
	printf("data at head before pushing: %d\n",
			*(int *)list->head->data);
	
	int data2 = 5;
	linkedlist_push(list, &data2);
	
	printf ("size of list (should be 2): %d\n", linkedlist_size(list));
	
	printf("data at head after pushing: %d\n",
			*(int *)list->head->data);
	printf("data at head->next after pushing: %d\n",
			*(int *)list->head->next->data);
	
	printf("data at head after popping: %d\n",
			*(int *)list->head->data);
	
	int addthese[] = {
		7, 8, 9, 10, 11, 12, 13
	};
	
	int i;
	for (i = 0; i < sizeof(addthese) / sizeof(addthese[0]); ++i) {
		linkedlist_add(list, &addthese[i]);
	}
	
	for (i = 0; i < linkedlist_size(list); ++i) {
		printf("list at %d: %d\n", i, *(int *)linkedlist_at(list, i));
	}
	
	destroy_linkedlist(list);
	
	return 0;
}
