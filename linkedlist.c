#include <stdlib.h>
#include <assert.h>
#include "linkedlist.h"

/*
	node
*/

node_t* new_node(void *data)
{
	node_t *node =  calloc(1, sizeof(node_t));
	node->data = data;
	return node;
}

void destroy_node(node_t *node)
{
	return free(node);
}

/*
	list acquisition and disposal
*/

linkedlist_t* new_linkedlist()
{
	linkedlist_t *list = calloc(1, sizeof(linkedlist_t));	
	return list;
}

void destroy_linkedlist(linkedlist_t *list)
{
	node_t *curnode = list->head;
	while (curnode) {
		node_t *nextnode = curnode->next;
		destroy_node(curnode);
		curnode = nextnode;
	}
	
	free(list);
	
	return;
}

/*
	stat
*/

size_t linkedlist_size(linkedlist_t *list)
{
	node_t *curnode = list->head;
	
	int i;
	for (i = 0; curnode; ++i) {
		curnode = curnode->next;
	}
	
	return i;
}

void* linkedlist_at(linkedlist_t *list, size_t index)
{
	assert(index < linkedlist_size(list));
	
	node_t *curnode = list->head;
	
	size_t i;
	for (i = 0; i < index; ++i) {
		curnode = curnode->next;
	}
	
	return curnode->data;
}

/*
	insertion and deletion
*/

void linkedlist_add(linkedlist_t *list, void *data)
{
	if (!list->head) {
		list->head = new_node(data);
		return;
	}
	
	node_t *curnode = list->head;
	while (curnode->next) {
		curnode = curnode->next;
	}
	
	curnode->next = new_node(data);
	
	return;
}

void linkedlist_insert(linkedlist_t *list, size_t index, void *data)
{
	assert(index < linkedlist_size(list));
	
	node_t *curnode = list->head;
	
	size_t i;
	for (i = 0; i < index; ++i) {
		curnode = curnode->next;
	}
	
	node_t *newnode = new_node(data);
	newnode->next = curnode->next;
	curnode->next = newnode;
	
	return;
}

void linkedlist_delete(linkedlist_t *list, size_t index, void *data)
{
	assert(index < linkedlist_size(list));
	
	node_t *curnode = list->head;
	
	size_t i;
	for (i = 0; i < index - 1; ++i) {
		curnode = curnode->next;
	}
	
	node_t *todelete = curnode->next;
	node_t *newnext = curnode->next->next;
	
	curnode->next = newnext;
	destroy_node(todelete);
	
	return;
}

void linkedlist_push(linkedlist_t *list, void *data)
{
	if (!list->head) {
		list->head = new_node(data);
		return;
	}
	
	node_t *oldhead = list->head;
	
	list->head = new_node(data);
	list->head->next = oldhead;
	
	return;
}

void linkedlist_pop(linkedlist_t *list)
{
	if (!list->head) {
		return;
	}
	
	if (!list->head->next) {
		return;
	}
	
	node_t *newhead = list->head->next;
	destroy_node(list->head);
	list->head = newhead;
	
	return;
}
