#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "node.h"

typedef struct linkedlist_t {
	node_t *head;
} linkedlist_t;

// acquisition and disposal

linkedlist_t* new_linkedlist();
void destroy_linkedlist(linkedlist_t*);

// stat

size_t linkedlist_size(linkedlist_t*);
void* linkedlist_at(linkedlist_t*, size_t);

// insertion and deletion

void linkedlist_add(linkedlist_t*, void*);
void linkedlist_insert(linkedlist_t*, size_t, void*);
void linkedlist_delete(linkedlist_t*, size_t, void*);
void linkedlist_push(linkedlist_t*, void*);
void linkedlist_pop(linkedlist_t*);

#endif // LINKEDLIST_H
